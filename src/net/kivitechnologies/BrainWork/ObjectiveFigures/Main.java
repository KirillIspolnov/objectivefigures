package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Класс, демонстрирующий возможности ООП на примере создания нескольких фигур и
 * нахождения самой большой по площади из них
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Main {

    /**
     * Основной метод, вызываемый JVM во время запуска программы
     *  
     * @param args массив аргументов, переданных в командной строке
     */
    public static void main(String[] args) {
        Figure[] figures = generateRandomFigures();
        int indexOfLargest = findIndexOfLargestFigure(figures);
        System.out.printf("Самая большая фигура - %d-ая по счету:%n%s%n", (indexOfLargest + 1), figures[indexOfLargest]);
    }
    
    /**
     * Генерирует псевдо-случайное число в диапазоне от min до max
     * 
     * @param min Минимальное число, которое может быть сгенерировано
     * @param max Максимальное число, которое уже не может быть сгенерировано
     * @return псевдо-случайное число в диапазоне от min до max
     */
    private static int random(int min, int max) {
        return (int)(Math.random() * (max - min) + min);
    }

    /**
     * Генерирует массив случайных фигур случайной длины от 10 до 15 элементов
     * А также выводит информацию о каждой из фигур во время генерации
     * 
     * @return массив случайно сгенерированных фигур
     */
    private static Figure[] generateRandomFigures() {
        Figure[] figures = new Figure[random(10, 15)];
        
        for(int i = 0; i < figures.length; i++) {
            double random = Math.random();
            
            if(random < 0.34) {
                int side = random(3, 15);
                figures[i] = new Square(side);
            } else if(random < 0.67){
                int radius = random(3, 15);
                figures[i] = new Circle(radius);
            } else {
                figures[i] = new Triangle(new Point(), 
                                          new Point(random(1, 10), random(3, 5)), 
                                          new Point(random(4, 8), random(1, 10)));
            }
            
            System.out.printf("Добавлена фигура:%n%s%n", figures[i]);
        }
        
        return figures;
    }
    
    /**
     * Возвращает индекс фигуры с самой большой площадью в массиве figures
     * 
     * @param figures массив фигур
     * @return индекс самой большой по площади фигуры в массиве
     */
    private static int findIndexOfLargestFigure(Figure[] figures) {
        int indexOfLargest = 0;
        for(int i = 1; i < figures.length; i++) {
            if(figures[indexOfLargest].getArea() < figures[i].getArea()) {
                indexOfLargest = i;
            }
        }
        
        return indexOfLargest;
    }
}
