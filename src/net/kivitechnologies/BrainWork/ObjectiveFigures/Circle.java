package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Класс, представляющий фигуру окружность
 * Имеет две характеристики, задаваемые лишь при инициализации объекта конструктором: 
 * <ul><li>координаты центра, представленные классом Point</li>
 *     <li>радиус</li></ul>
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Circle extends Figure {
    private static final String OBJECT_FORMAT = "Окружность радиусом %d с центром в точке %s имеет площадь %.3f";
    //координаты центра, представленные классом Point
    private Point center;
    //радиус
    private int radius;
   
    /**
     * Конструктор для создания новой окружности, 
     * центр которой находится в точке center,
     * радиус которой равен radius, а
     * цвет фигуры определяется аргументом color 
     * 
     * @param center точка, в которой находится центр окружности
     * @param radius радиус окружности
     * @param color  цвет окружности
     */
    public Circle(Point center, int radius, Color color) {
        super(color);
        this.center = center;
        this.radius = radius;
    }
    
    /**
     * Конструктор для создания новой окружности белого цвета, 
     * центр которой находится в начале координат, а
     * радиус которой равен radius
     * 
     * @param  radius радиус
     */
    public Circle(int radius) {
        this(new Point(), radius, Color.WHITE);
    }
    
    /**
     * Возвращает центр окружности
     * 
     * @return точка, в которой находится центр окружности
     */
    public Point getCenter() {
        return center;
    }
    
    /**
     * Возвращает радиус окружности
     * 
     * @return радиус окружности
     */
    public int getRadius() {
        return radius;
    }
    
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return String.format(OBJECT_FORMAT, radius, center, getArea());
    }
}
