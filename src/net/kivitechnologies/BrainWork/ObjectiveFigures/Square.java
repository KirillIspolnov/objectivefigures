package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Класс, представляющий фигуру квадрат
 * Имеет две характеристики, задаваемые лишь при инициализации объекта конструктором: 
 * <ul><li>координаты левого верхнего угла, представленные классом Point</li>
 *     <li>длина стороны квадрата</li></ul>
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Square extends Figure {
    private static final String OBJECT_FORMAT = "Квадрат со стороной %d, левый верхний угол которого находится по координатам %s имеет площадь %d";
    //координаты левого верхнего угла, представленные классом Point
    private Point leftTopCorner;
    //длина стороны квадрата
    private int side;
    
    /**
     * Конструктор для создания нового квадрата, 
     * левый верхний угол которого находится в точке leftTopCorner,
     * длина стороны которого равна side, а
     * цвет фигуры определяется аргументом color 
     * 
     * @param leftTopCorner точка, в которой находится левый верхний угол квадрата
     * @param side          длина стороны квадрата
     * @param color         цвет квадрата
     */
    public Square(Point leftTopCorner, int side, Color color) {
        super(color);
        this.leftTopCorner = leftTopCorner;
        this.side = side;
    }
    
    /**
     * Конструктор для создания нового квадрата белого цвета, 
     * левый верхний угол которого находится в начале координат, а
     * длина стороны которого равна side
     * 
     * @param side длина стороны квадрата
     */
    public Square(int side) {
        this(new Point(), side, Color.WHITE);
    }
    
    /**
     * Возвращает левый верхний угол квадрата
     * 
     * @return точка, в которой находится левый верхний угол квадрата
     */
    public Point getLeftTopCorner() {
        return leftTopCorner;
    }
    
    /**
     * Возвращает длину стороны квадрата
     * 
     * @return длина стороны
     */
    public int getSide() {
        return side;
    }
    
    @Override
    public double getArea() {
        return side * side;
    }
    
    @Override
    public String toString() {
        return String.format(OBJECT_FORMAT, side, leftTopCorner, (int)getArea());
    }

}
