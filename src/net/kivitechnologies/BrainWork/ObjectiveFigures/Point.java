package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Класс, представляющий точку в Декартовой системе коорднат
 * Каждая точка имеет две характеристики - положение по оси абсцисс и по оси ординат
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Point {
    private static final String OBJECT_FORMAT = "[%d, %d]";
    //Координата по оси абсцисс
    private int x;
    //Координата по оси ординат
    private int y;
    
    /**
     * Основной конструктор, используемый для создания точки в пространстве по координатам x и y
     * 
     * @param x координата по оси абсцисс
     * @param y координата по оси ординат
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Конструктор по умолчанию, создающий точку, находящуюся в начале координат 
     */
    public Point() {
        this(0, 0);
    }
    
    /**
     * Возвращает координату по оси абсцисс
     * 
     * @return координата по оси абсцисс
     */
    public int getX() {
        return x;
    }
    
    /**
     * Возвращает координату по оси ординат
     * 
     * @return координата по оси ординат
     */
    public int getY() {
        return y;
    }
    
    /**
     * Позволяет задать координату по оси абсцисс
     * 
     * @param x координата по оси абсцисс
     */
    public void setX(int x) {
        this.x = x;
    }
    
    /**
     * Позволяет задать координату по оси ординат
     * 
     * @param y координата по оси абсцисс
     */
    public void setY(int y) {
        this.y = y;
    }
    
    /**
     * Возвращает расстояние от текущей точки до точки, переданной в аргументы
     * 
     * @param other точка, расстояние до которой нужно найти
     * @return расстояние от текущей точки до точки other
     */
    public double countDistanceTo(Point other) {
        double x_sqr = Math.pow(this.getX() - other.getX(), 2);
        double y_sqr = Math.pow(this.getY() - other.getY(), 2);
        return Math.sqrt(x_sqr + y_sqr);
    }
    
    @Override
    public String toString() {
        return String.format(OBJECT_FORMAT, x, y);
    }
}