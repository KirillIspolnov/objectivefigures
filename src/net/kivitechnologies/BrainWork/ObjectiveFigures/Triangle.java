package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Класс, представляющий фигуру треугольник
 * Имеет три характеристики, объединенные в массив: координаты трех его вершин,
 * описываемые с помощью класса Point
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Triangle extends Figure {
    private static final String OBJECT_FORMAT = "Треугольник с вершинами %s, %s, %s и сторонами %.3f, %.3f, %.3f имеет площадь %.3f";
    //Массив вершин треугольника
    private Point[] vertexes;
   
    /**
     * Конструктор для создания треугольника, 
     * цвет которого устанавливается аргументом color, а
     * вершины - содержатся в массиве vertexes
     *  
     * @param vertexes массив вершин треугольника
     * @param color    цвет треуголника
     */
    public Triangle(Point[] vertexes, Color color) {
        super(color);
        this.vertexes = vertexes;
    }
    
    /**
     * Конструктор для создания треугольника, вершины которого заданы точками a, b и c
     * 
     * @param a Первая вершина треугольника
     * @param b Вторая вершина треугольника
     * @param c Третья вершина треугольника
     */
    public Triangle(Point a, Point b, Point c) {
        this(new Point[]{ a, b, c }, Color.WHITE);
    }
    
    /**
     * Возвращает длину первой стороны треугольника,
     * то есть стороны между первой и тертьей вершинами
     * 
     * @return длина стороны между первой и третьей вершинами
     */
    public double getFirstSide() {
        return vertexes[0].countDistanceTo(vertexes[1]);
    }
    
    /**
     * Возвращает длину второй стороны треугольника,
     * то есть стороны между второй и тертьей вершинами
     * 
     * @return длина стороны между второй и третьей вершинами
     */
    public double getSecondSide() {
        return vertexes[1].countDistanceTo(vertexes[2]);
    }
    
    /**
     * Возвращает длину третьей стороны треугольника,
     * то есть стороны между первой и тертьей вершинами
     * 
     * @return длина стороны между первой и третьей вершинами
     */
    public double getThirdSide() {
        return vertexes[0].countDistanceTo(vertexes[2]);
    }
    
    @Override
    public double getArea() {
        double first = getFirstSide(),
               second = getSecondSide(),
               third = getThirdSide();
        double p2 = (first + second + third) / 2;
        return Math.sqrt(p2 * (p2 - first) * (p2 - second) * (p2 - third));
    }
    
    @Override
    public String toString() {
        return String.format(OBJECT_FORMAT, vertexes[0], vertexes[1], vertexes[2],
                             getFirstSide(), getSecondSide(), getThirdSide(),
                             getArea());
    }
}
