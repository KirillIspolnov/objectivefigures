package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Класс, представляющий абстрактную фигуру. 
 * Дочерний класс должен определить метод getArea, возвращающий площадь фигуры
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public abstract class Figure {
    //Цвет фигуры
    private Color color;
    
    /**
     * Конструктор, создающий фигуру заданного с помощью аргумента color цвета
     * 
     * @param color цвет фигуры
     */
    public Figure(Color color) {
        this.color = color;
    }
    
    /**
     * Конструктор по умолчанию, создающий фигуру белого цвета
     */
    public Figure() {
        this(Color.WHITE);
    }
    
    /**
     * Возвращает цвет фигуры, представленный перечислением Color
     * 
     * @return цвет фигуры, представленный перечислением Color
     */
    public Color getColor() {
        return color;
    }
    
    /**
     * Позволяет задать цвет фигуры, представленный с помощью перечисления Color
     * 
     * @param color цвет фигуры, представленный перечислением Color
     */
    public void setColor(Color color) {
        this.color = color;
    }
    
    /**
     * Возвращает площадь фигуры
     * Метод объявлен абстрактным, т.к. для разных фигур
     * применяются разные алгоритмы вычисления площади 
     * 
     * @return площадь фигуры
     */
    public abstract double getArea();
}
