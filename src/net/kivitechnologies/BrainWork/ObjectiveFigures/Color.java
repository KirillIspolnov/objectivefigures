package net.kivitechnologies.BrainWork.ObjectiveFigures;

/**
 * Перечисление возможных цветов фигуры
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public enum Color {
    WHITE, BLACK, REG, GREEN, BLUE
}
